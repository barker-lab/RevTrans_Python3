# RevTrans_Python3

This is a modified version of [RevTrans v1.4](http://www.cbs.dtu.dk/services/RevTrans-2.0/web/download.php)
that is compatible with Python 3 (tested on Python v3.6). Most of the modifications are related to the newer
Python methods for working with dictionaries. We have also modified all `print` statements and `Exceptions`.
